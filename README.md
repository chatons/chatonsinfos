# ChatonsInfos

**ChatonsInfos** est un protocole de partage de données sur le collectif, ses membres et leurs services.

L’idée est de faire ça de façon simple et légère via des fichiers de description accessibles par lien web : 1 fichier par chaton et 1 fichier par service.

Maintenus en autonomie par les chatons volontaires, ces fichiers seront collectés régulièrement et valorisés dans le futur site [stats.chatons.org](https://stats.chatons.org/).

 En favorisant un partage d’information sur les chatons et leurs services, les résultats attendus sont :

* une meilleure visibilité des chatons et de leurs services ;
* une mesure de l’activité des services : même incomplète ou partielle, cela donnera idée de l’activité globale.


## BIEN COMMENCER

### Fonctionnement général

ChatonsInfos s'appuie sur StatoolInfos qui utilise des fichiers publics disponibles via des liens web :

- 1 fichier web public pour décrire la fédération ;
- 1 fichier web public par organisation membre de la fédération ;
- 1 fichier web public par service des organisations.

![Arborescence des fichiers properties](https://framagit.org/chatons/chatonsinfos/-/raw/master/schema-subs.svg)

Chaque fichier web public étant accessible via un lien web, chaque fichier public peut se trouver sur un site web différent.

### Je suis un chaton, par où commencer ?

Il faut créer et mettre en ligne :
1. un fichier de propriétés *organisation* pour votre chaton en basant sur le modèle pour les [organisations](https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/organization.properties),
2. un fichier de propriétés pour chaque *service* que vous proposez en utilisant [un des modèles service-*.properties](https://framagit.org/chatons/chatonsinfos/-/tree/master/MODELES) ou en se basant [sur ceux des autres chatons](https://stats.chatons.org/propertiesFiles.xhtml). 

### Où mettre les fichiers properties ?

Tous les fichiers properties doivent être accessibles publiquement sur le web. 

Par défaut, recommandation d'utiliser la convention des dossiers _.well_known_. Cela consiste à créer une arborescence _/.well_known/chatonsinfos/monfichier.properties_ sur un domaine web.

Quelques références :
* [List of /.well-known/ services offered by webservers via Wikipedia En](https://en.wikipedia.org/wiki/List_of_/.well-known/_services_offered_by_webservers)
* [Well-Known Uniform Resource Identifiers (URIs) via IETF](https://tools.ietf.org/html/rfc8615)

/i\ Si vous stockez ces fichiers sur Nextcloud, pensez à ajouter `download`à la fin de votre lien de partage.

### J'ai un lien pour le fichier properties de mon *organisation*, j'en fais quoi ?

Transmettre l'URL au collectif CHATONS par l'une des voies suivantes :
* courriel à contact@chatons.org 
* merge-request sur le fichier https://framagit.org/chatons/chatonsinfos/-/blob/master/StatoolInfos/chatons.properties pour ajouter/modifier une ligne dans la section « [Subs] ».

### J'ai un lien pour le fichier properties d'un *service*, j'en fais quoi ?

Ajouter cette URL dans le fichier _properties_ de votre organisation, section « [Subs] ».


## PLAN DE LA DOCUMENTATION

* README.md : accueil ;
* CONCEPTS.md : les concepts fondamentaux ;
* FAQ.md : une foire aux questions ;
* MODELES/federation.properties : modèle de fihcier _properties_ pour le collectif CHATONS ;
* MODELES/organization.properties : modèle de fichier _properties_ pour un membre du collectif CHATONS ;
* MODELES/services.properties : modèle de fichier _properties_ générique pour un service logiciel d'un membre du collectif CHATONS ;
* MODELES/services-xxxxx.properties : modèle de fichier _properties_ d'un logiciel précis, utile pour les métriques spécifiques ;
* StatoolInfos/ : dossier de configuration de l'outil StatoolInfos, le générateur des pages de [stats.chatons.org](https://stats.chatons.org/).
* WorkingGroup/ : dossier de sauvegarde des réunions du groupe de travail.


## LICENCE

Sauf mention contraire, les documents du projet ChatonsInfos sont diffusés sous la licence libre Creative Commons CC-BY-SA.


## AUTEUR

Le collectif CHATONS.


## LOGO

* fichier : logo_ChatonsInfos.jpg ;
* source : projet StatoolInfos ;
* auteur: Christian Pierre MOMON christian.momon@devinsy.fr
* licence: Creative Commons CC-BY-SA last version.
